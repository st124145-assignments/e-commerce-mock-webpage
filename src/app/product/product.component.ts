import { Component } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {
  isProductInStock = true
  studentId = "Type something to see my student Id"
  stock = 10

  Product = {
    bottle: "assets/water-bottle.jpg"
  }

  whenChanged(){
    this.studentId = "st124145"
  }

  whenClicked() {
    this.stock -= 1;
    if (this.stock === 0) {
      this.isProductInStock = false;
    }
  }
  
}
